﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Common.Models.Crm
{
    public class ListenedEntity : ListenedBase
    {
        [JsonProperty(PropertyName = "leads")]
        public ListenedEvent Leads { set => ProperyName = new JProperty("Entity", "lead"); }

        [JsonProperty(PropertyName = "contacts")]
        public ListenedEvent Contacts { set => ProperyName = new JProperty("Entity", "contacts"); }

        [JsonProperty(PropertyName = "task")]
        public ListenedEvent Task { set => ProperyName = new JProperty("Entity", "task"); }

        [JsonProperty(PropertyName = "account")]
        public ListenedEvent Account { set => ProperyName = new JProperty("Entity", "account"); }
    }
}
