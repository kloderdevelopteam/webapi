﻿using Newtonsoft.Json.Linq;

namespace WebApi.Common.Models.Crm
{
    public class EventObjectBuilder
    {
        JObject result;

        public JObject GetEventObject { get => result; }

        public void SetEntityHandle(object sender, ListenedArguments e)
        {
            result.Add("Entity", e.Property);
        }

        public void SetEventHandle(object sender, ListenedArguments e)
        {
            result.Add("Event", e.Property);
        }
        public void SetEntityIdHandle(object sender, ListenedArguments e)
        {
            result.Add("Id", e.Property);
        }
        public void SetAccountHandle(object sender, ListenedArguments e)
        {
            result.Add("Account", e.Property);
        }
    }
}
