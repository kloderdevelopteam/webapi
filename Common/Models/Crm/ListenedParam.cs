﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Common.Models.Crm
{
    public class ListenedParam : ListenedBase
    {
        [JsonProperty(PropertyName = "id")]
        public Int32 Id { set => ProperyName = new JProperty("EntityId", value); }
    }
}
