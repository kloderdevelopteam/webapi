﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Common.Models.Crm
{
    public class ListenedEvent : ListenedBase
    {
        [JsonProperty(PropertyName = "add")]
        public IEnumerable<ListenedParam> Add { set => ProperyName = new JProperty("Event", "add"); }

        [JsonProperty(PropertyName = "update")]
        public IEnumerable<ListenedParam> Update { set => ProperyName = new JProperty("Event", "update"); }

        [JsonProperty(PropertyName = "status")]
        public IEnumerable<ListenedParam> Status { set => ProperyName = new JProperty("Event", "status"); }

        [JsonProperty(PropertyName = "note")]
        public IEnumerable<ListenedParam> Note { set => ProperyName = new JProperty("Event", "note"); }

        //[JsonProperty(PropertyName = "subdomain")]
        //public string Subdomain { set => ProperyName = new JProperty("Event", "subdomain"); }
    }
}
