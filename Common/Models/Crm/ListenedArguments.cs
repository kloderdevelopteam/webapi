﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Common.Models.Crm
{
    public class ListenedArguments : EventArgs
    {
        string name;

        public ListenedArguments(string s)
        {
            name = s;
        }

        public string Property
        {
            get { return name; }
        }
    }
}
