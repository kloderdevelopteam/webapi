﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using WebApi.Common.Models.Crm;

namespace WebApiFPA.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        ILogger logger;

        public ValuesController()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.Seq("http://logs.fitness-pro.ru:5341")
                .CreateLogger();
        }


        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            Log.Information("Hello, {Name}!", Environment.UserName);

            Log.Information("Test log some object into Seq, {@person}!", new { Name = "Илья", LastName = "Иджян", Gender = "Male" });

            return new string[] { "value1", "value2" };
            
        }


        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        //public HttpResponseMessage Post()
        public HttpResponseMessage Post(CrmEvent value)
        {
            var ev = new ListenedEntity();
            var t = ev.Task = new ListenedEvent();

            var ttt = new StreamReader(Request.Body).ReadToEndAsync().Result;

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Seq("http://logs.fitness-pro.ru:5341")
                .CreateLogger();


            Log.Information("Входящее событие, {@Element}!", value);

            var api = new MandrillApi("3GWqeBmS1HGZy9Y8FOzKJQ");
            var message = new MandrillMessage("info@fitness-pro.ru", "kloder@fitness-pro.ru",
                            "hello mandrill!", "...how are you?");
            //var result = await api.Messages.SendAsync(message);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }


    public class CrmEvent
    {
        public CrmEntity leads { get; set; }
        public CrmEntity contacts { get; set; }
        public CrmEntity task { get; set; }
        public CrmEntity account { get; set; }
    }


    public class CrmEntity
    {
        public IEnumerable<CrmEntityParams> add { get; set; }
        public IEnumerable<CrmEntityParams> update { get; set; }
        public IEnumerable<CrmEntityParams> status { get; set; }
        public IEnumerable<CrmEntityParams> note { get; set; }
        public string subdomain { get; set; }
    }

    public class CrmEntityParams {
        public Int32 id { get; set; }
        public Int32 responsible_user_id { get; set; }
    }
}
