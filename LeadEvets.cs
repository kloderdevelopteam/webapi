﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiFPA
{
    public class LeadEvets
    {
        IEnumerable<EventParams> add { get; set; }
        IEnumerable<EventParams> update { get; set; }
        IEnumerable<EventParams> status { get; set; }
    }
}
